#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

sshd_config="/etc/ssh/sshd_config"

if [ $(id -u) -ne 0 ]; then
	printf -- "This script requires root privileges.\n" 1>&2
	exit 1
fi

sed -i '/^\s*PasswordAuthentication/d' "${sshd_config}" 
sed -i '/^\s*PermitRootLogin/d'        "${sshd_config}" 
sed -i '/^\s*PubkeyAuthentiation/d'    "${sshd_config}" 

tee -a "${sshd_config}" <<_EOT_ >/dev/null
PubkeyAuthentication yes
PasswordAuthentication no
PermitRootLogin no
_EOT_

systemctl restart sshd.service

# [EOF]
