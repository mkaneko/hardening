#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -u

docroot="/var/www/html"

## Suspicious file search
printf -- "==================== [$(hostname)] [SUSPICIOUS] [BEGIN] ====================\n"
find "${docroot}" -type f -and '(' \
	-iname "*.dump" \
	-o -iname "*.dmp" \
	-o -iname "*.sql*" \
	-o -iname "*.bak" \
	-o -iname "*.pcap" \
	-o -iname "*backup*" \
	-o -iname "welcart-date" ')'
printf -- "==================== [$(hostname)] [SUSPICIOUS] [ END ] ====================\n"

## Backdoor search
printf -- "==================== [$(hostname)] [BACKDOOR] [BEGIN] ====================\n"
find "${docroot}" -type f \
	-iname "*.php" \
	-o -iname "*.cgi" \
	-print0 \
	| xargs -0 grep -l '(system|passthru|proc_open|shell_exec)'
printf -- "==================== [$(hostname)] [BACKDOOR] [ END ] ====================\n"

## Search for files over 10 MB in size
printf -- "==================== [$(hostname)] [OVER-10MB] [BEGIN] ====================\n"
find "${docroot}" -type f -size +10240k
printf -- "==================== [$(hostname)] [OVER-10MB] [ END ] ====================\n"

## Search for files updated or created within 10 minutes
printf -- "==================== [$(hostname)] [MODIFIED-10MIN] [BEGIN] ====================\n"
find "${docroot}" -type f -mmin -10
printf -- "==================== [$(hostname)] [MODIFIED-10MIN] [ END ] ====================\n"

# [EOF]
