#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

: ${1} ${2} ${3}

target_host_list="${1}"
fpath_source="${2}"
username="${3}"
sftp_opts="-oPort=22"

for target_host in $(cat ${target_host_list})
do
	sftp "${sftp_opts}" "${username}@${target_host}" <<_EOT_
mkdir sftp-uploads
cd sftp-uploads
put ${fpath_source}
exit
_EOT_
done

# [EOF]
