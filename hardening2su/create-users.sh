#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

password="XXXXXXXXXX"
users="testuser1 testuser2 testuser3"

if [ $(id -u) -ne 0 ]; then
	printf -- "This script requires root privileges.\n" 1>&2
	exit 1
fi

for username in ${users}
do
	exec 2>/dev/null
	useradd -N -m "${username}"
	usermod -aG root "${username}"  || true
	usermod -aG wheel "${username}" || true
	usermod -aG sudo  "${username}" || true
	printf -- "${password}\n${password}\n" | passwd "${username}"
	id "${username}"
done

# [EOF]
