#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

: ${1} ${2} ${3}

target_hosts="${1}"
pubkey="${2}"
username="${3}"
sftp_opts="-oPort=22"

tmpdir=$(mktemp -d)

chmod 700 "${tmpdir}"
cp "${pubkey}" "${tmpdir}/"

for target_host in $(cat ${target_hosts})
do
	authkeys_new="$(/bin/sh -c 'echo $$').txt"
	sftp "${sftp_opts}" "${username}@${target_host}" <<_EOT_
mkdir .ssh
chmod 700 .ssh
lcd ${tmpdir}
get .ssh/authorized_keys
!touch authorized_keys
!cd ${tmpdir} && cat ${pubkey} authorized_keys >> ${authkeys_new}
put ${authkeys_new} .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
quit
_EOT_
done

rm -rf "${tmpdir}"

# [EOF]
