#!/bin/sh
PATH=/bin:/usr/bin:/sbin:/usr/sbin; export PATH
set -eu

password="XXXXXXXXXX"

if [ $(id -u) -ne 0 ]; then
	printf -- "This script requires root privileges.\n" 1>&2
	exit 1
fi

## userXX
for username in $(cut -d':' -f1 /etc/passwd | grep '^user[0-9][0-9]*')
do
	printf -- "${password}\n${password}\n" | passwd "${username}"
	usermod -s /bin/false "${username}"
done

## root
printf -- "${password}\n${password}\n" | passwd "root"

# [EOF]
